## Instruction for Interviewer
1. Open the JavaScript repls 
2. Delete the Solution files.
3. Check that the repls with HTML run correctly

## JavaScript
1. [Question 1](https://repl.it/@destiny_index/StringLengthTest)
2. [Question 2](https://repl.it/@destiny_index/MakePeopleTest)
3. [Question 3](https://repl.it/@destiny_index/ExpandingContainerTest)
4. [Question 4](https://repl.it/@destiny_index/PeopleButtonsTest)

## SQL
1. Open [sqliteonline](https://sqliteonline.com)
2. Load the DB File 
    - File -> Open DB -> [.db File](./SQLTest/books.db)
3. Answer the [Questions](./SQLTest/README.md)

