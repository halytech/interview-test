import { toggleClass } from './toggleClass.js'
 
const myButton = document.getElementById('my-button')
const myContainer = document.getElementById('my-container')

myButton.onclick = function (event) {
  event.stopPropagation() // Solution -> Call event.stopPropagation
  toggleClass(myContainer, 'dark')
}

myContainer.onclick = function () {
  toggleClass(myContainer, 'expanded')
}