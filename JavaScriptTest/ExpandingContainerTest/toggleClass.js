export function toggleClass (element, toToggle) {
  const classes = element.className.split(' ')
  
  if (classes.includes(toToggle))
    element.className = classes.filter(name => name !== toToggle).join(' ')
  else
    element.className = [...classes, toToggle].join(' ')
}
