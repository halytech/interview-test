/* There is a container which expands/shrinks when clicked, and a button
 * which toggles the background-color of the container.
 *
 * 1. Why does the container expand/shrink when the button is pressed?
 * 
 * 2. Change the code below so that the container does not expand/shrink
 *    when the button is clicked.
 */
import { toggleClass } from './toggleClass.js'
 
const myButton = document.getElementById('my-button')
const myContainer = document.getElementById('my-container')

myButton.onclick = function () {
  toggleClass(myContainer, 'dark')
}

myContainer.onclick = function () {
  toggleClass(myContainer, 'expanded')
}