/* There are four buttons corresponding to four people. Each 
 * button should alert a message saying:
 *
 * 'NAME is AGE years old'
 * 
 * e.g. Button 3 alerts 'jack is 30 years old'
 *
 * 1. Why doesn't the following code work as intended?
 *
 * 2. Rewrite the code below to fix this problem. Only use ES5.
 *    Bonus marks if you can get rid of any `for` or `while` loops
 *    
 * 3. How might you optimize for the case where there are 100000
      people and buttons
 */
 
var people = [
  { name: 'bob', age: 10 },
  { name: 'frank', age: 20 },
  { name: 'jack', age: 30 },
  { name: 'jill', age: 40 },
  { name: 'oops', age: NaN }
]

var buttons = document.getElementById('button-set').children

for (var i = 0; i < buttons.length; i++) {
  buttons[i].onclick = function () {
    alert(people[i].name + ' is ' + people[i].age + ' years old')
  }
}