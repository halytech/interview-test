/* 1. `var i` does not have block scope, so all the alerts are using
 *    the final value of i = 4
 * 
 * 3. Use Event Delegation. Put a single click handler on the parent
 *    element of the buttons and check the event target to find out
 *    which button was clicked.
 */
var people = [
  { name: 'bob', age: 10 },
  { name: 'frank', age: 20 },
  { name: 'jack', age: 30 },
  { name: 'jill', age: 40 },
  { name: 'oops', age: NaN }
]

var buttons = document.getElementById('button-set').children

// 2. Solution using IIFE (ES3 style)
for (var i = 0; i < buttons.length; i++) {
  (function (i) {
    buttons[i].onclick = function () {
      alert(people[i].name + ' is ' + people[i].age + ' years old')
    }
  }(i))
}

// 2. Solution using Array.prototype.map.call (ES5 style)
Array.prototype.map.call(buttons, function (button, i) {
  button.onclick = getClickHandler(people[i])
})

function getClickHandler (person) {
  return function () {
    alert(person.name + ' is ' + person.age + ' years old')
  }
}