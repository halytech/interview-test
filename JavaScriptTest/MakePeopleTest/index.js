const assert = require('assert')
const makePeople = require('./makePeople')

try {
	const names = 'bob frank jack jill'
	const ages = '10 20 30 40'

	assert.deepStrictEqual(makePeople(names, ages), [
		{ name: 'bob', age: 10 },
		{ name: 'frank', age: 20 },
		{ name: 'jack', age: 30 },
		{ name: 'jill', age: 40 }
	])

	console.log('Pass')
} catch (err) {
	console.error({ expected: err.expected, actual: err.actual })
}
