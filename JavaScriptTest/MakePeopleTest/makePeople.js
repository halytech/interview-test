/* Given a string containing names and a string containing ages,
 * merge the corresponding names and ages from each string into
 * an object.
 *
 * e.g.
 *
 * const names = 'bob frank jack jill'
 * const ages = '10 20 30 40'
 * 
 * const expected = [
 *   { name: 'bob', age: 10 },
 *   { name: 'frank', age: 20 },
 *   { name: 'jack', age: 30 },
 *   { name: 'jill', age: 40 }
 * ]
 */

module.exports = function makePeople (names, ages) {
  // Solution goes here
}
