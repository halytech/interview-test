module.exports = function makePeople (names, ages) {
  ages = ages.split(' ').map(Number)
  names = names.split(' ')
  
  return names.map((name, i) => ({name, age: ages[i]}))
}