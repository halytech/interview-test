-- 1. Profit per Book
SELECT title, (price - cost) * qty_sold AS profit
FROM books;

-- 2. Profit per Author
SELECT name, SUM((price - cost) * qty_sold) AS profit 
FROM books
INNER JOIN authors ON author = authors.id
GROUP BY author;

-- 3. Author with Highest Profit together with Profit
SELECT name, MAX(profit) as profit
FROM (
	SELECT name, SUM((price - cost) * qty_sold) AS profit 
	FROM books
	INNER JOIN authors ON author = authors.id
	GROUP BY author
);
