## Question 1
Write a query to display the profit per book. Profit is given by (price - cost) * qty\_sold

| title                                                              | profit      |
| ------------------------------------------------------------------ | ----------- |
| Clean Code: A Handbook of Agile Software Craftsmanship             | 91515       |
| Agile Software Development: Principles, Patterns, and Practices    | 35913.59999 |
| Refactoring: Improving the Design of Existing Code                 | 342441      |
| Patterns of Enterprise Appllication Architecture                   | 10895.87999 |
| Working Effectively with Legacy Code                               | 119697      |
| Extreme Programming Explained                                      | 74848.5     |

## Question 2
Write a query to display the profit per author

| name             | profit       |
| ---------------- | ------------ |
| Robert C. Martin | 127428.59999 |
| Martin Fowler    | 353336.88    |
| Michael Feathers | 119697       |
| Kent Beck        | 74848.5      |

## Question 3
Write a query to display the author with the highest profit, together with the profit

| name          | profit    |
| ------------- | --------- |
| Martin Fowler | 353336.88 |

