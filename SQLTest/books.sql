CREATE TABLE authors (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL
);

CREATE TABLE books (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	title TEXT NOT NULL,
	author INTEGER NOT NULL,
	price REAL NOT NULL,
	cost REAL NOT NULL,
	qty_sold INTEGER NOT NULL,
	FOREIGN KEY(author) REFERENCES authors(id)
);

INSERT INTO authors (name) VALUES ('Robert C. Martin');
INSERT INTO authors (name) VALUES ('Martin Fowler');
INSERT INTO authors (name) VALUES ('Michael Feathers');
INSERT INTO authors (name) VALUES ('Kent Beck');

INSERT INTO books (title, author, price, cost, qty_sold) VALUES (
	'Clean Code: A Handbook of Agile Software Craftsmanship',
	(SELECT id FROM authors WHERE name = 'Robert C. Martin'),
	13.00,
	10.00,
	30505
);

INSERT INTO books (title, author, price, cost, qty_sold) VALUES (
	'Agile Software Development: Principles, Patterns, and Practices',
	(SELECT id FROM authors WHERE name = 'Robert C. Martin'),
	11.20,
	8.00,
	11223
);

INSERT INTO books (title, author, price, cost, qty_sold) VALUES (
	'Refactoring: Improving the Design of Existing Code',
	(SELECT id FROM authors WHERE name = 'Martin Fowler'),
	16.50,
	11.00,
	62262
);

INSERT INTO books (title, author, price, cost, qty_sold) VALUES (
	'Patterns of Enterprise Appllication Architecture',
	(SELECT id FROM authors WHERE name = 'Martin Fowler'),
	18.99,
	10.00,
	1212
);

INSERT INTO books (title, author, price, cost, qty_sold) VALUES (
	'Working Effectively with Legacy Code',
	(SELECT id FROM authors WHERE name = 'Michael Feathers'),
	18.00,
	15.00,
	39899
);

INSERT INTO books (title, author, price, cost, qty_sold) VALUES (
	'Extreme Programming Explained',
	(SELECT id FROM authors WHERE name = 'Kent Beck'),
	18.00,
	16.50,
	49899
);
